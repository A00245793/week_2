import junit.framework.TestCase;


public class NewAnalyseStringTest extends TestCase {

	/*
	 Test #: 		1
	 Objective: 	Verify length of string
	 Input(s): 		Bananas
	 Expected o/p: 	7
	 */
	public void testgetStringLen()
	{
		assertEquals(7, NewAnalyseString.getStringLen("Bananas"));
	}
	

	/*
	 Test #: 		2
	 Objective: 	Verify # vowels in string
	 Input(s): 		Bananas
	 Expected o/p: 	3
	 */
	public void testgetStringVow()
	{
		assertEquals(3, NewAnalyseString.getStringVow("Bananas"));
	}
	

	/*
	 Test #: 		3
	 Objective: 	Verify first letter of string
	 Input(s): 		Bananas
	 Expected o/p: 	B
	 */
	public void testgetStringFirst()
	{
		assertEquals('B', NewAnalyseString.getStringFirst("Bananas"));
	}
	

	/*
	 Test #: 		4
	 Objective: 	Verify last letter of string
	 Input(s): 		Bananas
	 Expected o/p: 	s
	 */
	public void testgetStringLast()
	{
		assertEquals('s', NewAnalyseString.getStringLast("Bananas"));
	}

}
