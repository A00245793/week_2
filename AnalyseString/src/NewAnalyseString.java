import java.util.Scanner;
public class NewAnalyseString {
	
	static int getStringLen(String str)
	{
		return str.length();
	}
	
	static int getStringVow(String str)
	{
		int count = 0;
		for (int i = 0; i < str.length(); i++)
		{
			switch(str.toLowerCase().charAt(i))
			{
				case 'a' :
				case 'e' :
				case 'i' :
				case 'o' :
				case 'u' :
					count++;
					break;
			}
		}
		return count;
	}
	
	static char getStringFirst(String str)
	{
		return str.charAt(0);
	}
	
	static char getStringLast(String str)
	{
		return str.charAt(str.length() - 1);
	}
	
	public static void main (String [] args)
	{
		Scanner in = new Scanner(System.in);
		System.out.print("Please enter a word: ");
		String wrd = in.nextLine();
		
		System.out.println("Your word has " + getStringLen(wrd) + " letters.");
		System.out.println("The number of vowels is " + getStringVow(wrd) + ".");
		System.out.println("The first letter is " + getStringFirst(wrd) + ".");
		System.out.println("The last letter is " + getStringLast(wrd) + ".");
		
	}

}
